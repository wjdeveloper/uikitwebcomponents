
export interface IUser {
    name: string;
    email?: string;
    avatar: string;
}

export interface IComment {
    user: IUser;
    comment: string;
}

export interface ILocation {
    lng: number;
    lat: number;
}

export class Post {


    constructor(
        public id: any,
        public user: IUser,
        public location: string,
        public images: any[],
        public likes: number,
        public comments: IComment[],
        public title: string,
        public create: string,
        public description: string,
    )
    {}

}