import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';

import { map } from 'rxjs/operators'
import { Observable } from 'rxjs';

import { IUser, Post, ILocation } from './post-model';

import * as moment from 'moment'
moment.locale('es');

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocationToAddres, Properties } from './location.interface';
import { Database } from './database';

@Injectable({ providedIn: 'root' })
export class ReleaseService {

    date = moment().format('YYYYMMDD hh:mm:ss a');
    lang: string = window.navigator.language.split("-")[0];
    form: NgForm;

    constructor(
        private _idb: Database,
        private _http: HttpClient
    )
    {}

    async createPost(form: NgForm)
    {
        this.form = form;

        const user: IUser = {
            name: this.form.value.userName,
            avatar: this.form.value.userPhoto
        }
        
        /**
         * Separate images string by ; to get and arrray of tree images
         * new array create @separateImage
         */
        const images: string = this.form.value.images;
        const separateImage: string[] = images.split(";");

        /**
         * Get user position GPS lng and lat
         */
        const location: ILocation = await this.getPosition();

        /**
         * Api Location to addres called
         * https://rapidapi.com/geoapify-gmbh-geoapify/api/location-to-address/endpoints
         */
        this.getLocation(location.lng, location.lat, 1, this.lang)
            .subscribe( (location: Properties) => {
                 let post = new Post(Date.now(), user, location.address_line2, separateImage, 0, [], this.form.value.title, this.date, this.form.value.description);
                 this._idb.save(post);
            });
        
    }

    getPosition(): Promise<ILocation> 
    {
        return new Promise( (resolve, reject) => {
            navigator.geolocation.getCurrentPosition( resp => {
                resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
            }, error => {
                reject('error');
            });
        });
    }

    getLocation(lng, lat, limit, lang): Observable<Properties>
    {
        const headers = new HttpHeaders()
             .set('x-rapidapi-key', 'ed0048b631mshdf6022a05fdb804p1a0576jsn369b88c93d4b')
             .set('Access-Control-Allow-Origin', '*');

        return this._http.get(`https://location-to-address.p.rapidapi.com/v1/geocode/reverse?lang=${lang}&lat=${lat}&lon=${lng}&limit=${limit}`, { headers: headers })
            .pipe(
                map( (data: LocationToAddres) => data.features[0].properties )
            );
    }


}