import { Subject } from 'rxjs';
import * as UIkit from 'uikit';
import * as moment from 'moment';
moment.locale('es');

import { Post } from './post-model';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class Database {

    db: any;
    idb: any = window.indexedDB.open('postsList', 3);

    posts: Post[] = [];
    postsChange = new Subject<Post[]>();

    constructor(
        private _router: Router
    ) {

        if ( !( 'indexedDB' in window ) ) {
            console.log('This browser doesn\'t support IndexedDB');
            return;
        } 
        
        this.idb.onsuccess = (event: Event) => {
            this.db =  this.idb.result;
            console.log('read an exist object store');

            this.fetchAll();
        }

        this.idb.onupgradeneeded = (event: Event) => {
            this.db =  this.idb.result;
            console.log('making a new object store');
            const objectStore = this.db.createObjectStore('posts', {
                keyPath: 'id'
            }); // Almacen
        }

        this.idb.onerror = (error) => {
            console.log('error: ', error);
        }
       
    }

    save = (post: Post) => {
        const transaction = this.db.transaction(['posts'], 'readwrite');
        const objectStore = transaction.objectStore('posts');
        const request = objectStore.add(post);

        request.onsuccess = () => {
            this.posts.push(post);
            this.postsChange.next(this.posts.slice());
            this._router.navigate(['/']);
            UIkit.notification({message: '<span uk-icon=\'icon: check\'></span> Se agrego un nuevo post', status: 'success', pos: 'top-left'});
        }


    }


    fetchAll = () => {
        const transaction = this.db.transaction(['posts'], 'readonly');
        const objectStore = transaction.objectStore('posts');
        const request = objectStore.openCursor();

        request.onsuccess = (e) => {
            const cursor = e.target.result;
            if ( cursor ) {

                // Change create to moment js
                cursor.value.create = moment(cursor.value.create, 'YYYYMMDD hh:mm:ss a').fromNow();
                this.posts.push(cursor.value);
                cursor.continue();
            } else {
                // console.log('There\'rent not more posts!')
                this.postsChange.next(this.posts.slice());
            }
        }
    }
    
    getPosts = () => {
        return this.posts;
    }

}