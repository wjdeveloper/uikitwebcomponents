import { Component, OnInit } from '@angular/core';
import { Post } from './post-card.interface';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {

  user: Post = {
    firstName: 'bill',
    lastName: 'gates',
    userName: 'bill.gates.19',
    following: true,
    followingMe: true,
    pvMember: true,
    create: '1 semana',
    title: 'The investments we make in primary health care in Africa can:',
    description: '- make communities far healthier - achieve health equity - stop the next pandemic before it gets large',
    works: {
      company: 'Co-chair, Bill & Melinda Gates Foundation'
    },
    address: {
      city: 'Seattle',
      state: 'Washington'
    },
    avatar: {
      profile: 'https://media-exp1.licdn.com/dms/image/C5603AQHv9IK9Ts0dFA/profile-displayphoto-shrink_200_200/0?e=1607558400&v=beta&t=FrXoNaVPiM138Ah-w8mYmd0hUeaJg-p2DZbIZgzq45s',
      cover: 'https://media-exp1.licdn.com/dms/image/C4D16AQGeHmoGyMRF2A/profile-displaybackgroundimage-shrink_200_800/0?e=1607558400&v=beta&t=hs1UfB1VJfX_TOdzkru_fuykX0rz7YCBpCdTGYgctaA'
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
