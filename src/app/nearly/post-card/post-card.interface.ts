

export interface Post
{
    firstName: string;
    lastName: string;
    userName: string;
    following: boolean;
    followingMe: boolean;
    create: string;
    pvMember: boolean;
    title: string;
    description: string;
    works: Works;
    address: Address;
    avatar: Avatar;
}

export interface Works
{
    company: string;
}

export interface Address
{
    city: string;
    state: string;
}

export interface Avatar
{
    profile: string;
    cover: string;
}