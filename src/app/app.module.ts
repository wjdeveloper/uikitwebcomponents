import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

/**
 * Services
 */
import { ReleaseService } from './post-service';
import { Database } from './database';

/**
 * Routes
 */
import { AppRoutingModule } from './app-routing.module';

/**
 * Pages
 */
import { PagesComponent } from './pages/pages.component';
import { PostsComponent } from './pages/posts/posts.component';
import { AddPostComponent } from './pages/add-post/add-post.component';
import { LoginComponent } from './pages/login/login.component';

/**
 * Components
 */
import { HeaderComponent } from './components/header/header.component';
import { CardPostComponent } from './components/card-post/card-post.component';
import { FormPostComponent } from './components/form-post/form-post.component';
import { FormLoginComponent } from './components/form-login/form-login.component';

/**
 * Directives
 */
import { ToggleAnimation } from './directives/toggle-animation.directive';
import { PostCardComponent } from './nearly/post-card/post-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PagesComponent,
    PostsComponent,
    AddPostComponent,
    LoginComponent,
    CardPostComponent,
    FormPostComponent,
    FormLoginComponent,
    ToggleAnimation,
    PostCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    ReleaseService,
    Database
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
