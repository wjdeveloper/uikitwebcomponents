import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages/pages.component';
import { PostsComponent } from './pages/posts/posts.component';
import { AddPostComponent } from './pages/add-post/add-post.component';
import { LoginComponent } from './pages/login/login.component';


const routes: Routes = [
    // { path: '', redirectTo: 'posts', pathMatch: 'full' },
    { 
        path: '', 
        component: PagesComponent,
        children: [
            { path: '', component: PostsComponent },
            { path: 'add-post', component: AddPostComponent },
        ] 
    },
    { path: 'login', component: LoginComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
