import { Component, OnInit } from '@angular/core';

import { Post } from '../../post-model';
import { Database } from '../../database';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts: Post[] = [];

  constructor( private _db: Database ) { }

  ngOnInit(): void {

    this.posts = this._db.getPosts();

    this._db.postsChange.subscribe( (posts: Post[]) => {
      this.posts = posts;
    });

  }

} 
