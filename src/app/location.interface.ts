export interface LocationToAddres {
    type: string;
    features: Feature[];
  }
  
export interface Feature {
    type: string;
    geometry: Geometry;
    properties: Properties;
  }
  
export interface Properties {
    housenumber: string;
    street: string;
    distance: number;
    country: string;
    county: string;
    datasource: Datasource;
    country_code: string;
    state: string;
    lon: number;
    lat: number;
    result_type: string;
    name: string;
    postcode: string;
    city: string;
    formatted: string;
    address_line1: string;
    address_line2: string;
    rank: Rank;
  }
  
export interface Rank {
    popularity: number;
  }
  
export interface Datasource {
    sourcename: string;
    osm_type: string;
    osm_id: number;
    continent: string;
  }
  
export interface Geometry {
    type: string;
    coordinates: number[];
  }