import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ReleaseService } from '../../post-service';

@Component({
  selector: 'app-form-post',
  templateUrl: './form-post.component.html',
  styleUrls: ['./form-post.component.scss']
})
export class FormPostComponent implements OnInit {

  constructor(private _rs: ReleaseService) { }

  ngOnInit(): void 
  {
  }

  onSubmit(form: NgForm): void 
  {
    this._rs.createPost(form);
  }

}
