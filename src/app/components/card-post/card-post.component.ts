import { Component, OnInit, Input } from '@angular/core';
import { Post, IUser } from '../../post-model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-card-post',
  templateUrl: './card-post.component.html',
  styleUrls: ['./card-post.component.scss']
})
export class CardPostComponent implements OnInit {

  @Input() post: Post;
  @Input() index: number;

  toggle: boolean = true;
  isTokenValid: boolean = false; 

  comment: { user: IUser, comment: string }[]= [];

  constructor()
  {}

  ngOnInit(): void
  {}

  onSubmit(form: NgForm) {


    console.log(form);

  }

}
